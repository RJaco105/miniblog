package com.mitocode.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mitocode.model.Persona;
import com.mitocode.model.Publicacion;
import com.mitocode.model.Usuario;
import com.mitocode.service.IPublicacionService;
import com.mitocode.util.MensajeManager;

@Named
@ViewScoped
public class PublicacionBean implements Serializable{

	@Inject
	private IPublicacionService service;
	@Inject
	private MensajeManager mensajeManager;
	@Inject
	private PushBean push;
	private List<Publicacion> publicaciones;
	private Publicacion publicacion;
	private Usuario us;
	
	@PostConstruct
	public void init() {
		this.publicacion = new Publicacion();

		FacesContext context = FacesContext.getCurrentInstance();
		this.us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");

		this.listarPublicaciones();		
	}
	
	public void publicar() {
		try {

			Persona p = new Persona();
			p.setIdPersona(us.getPersona().getIdPersona());
			this.publicacion.setCuerpo(this.publicacion.getCuerpo().trim());
			this.publicacion.setPublicador(p);
			int rpta = this.service.registrar(this.publicacion);
			if (rpta == 1) {
				mensajeManager.mostrarMensaje("AVISO", "Se public�", "INFO");
				push.sendMessage();	
			} else {
				mensajeManager.mostrarMensaje("AVISO", "No se public�", "WARN");
			}			
		} catch (Exception e) {
			//mensajeManager.mostrarMensaje("AVISO", "Error al publicar", "ERROR");
			System.out.println(e.getMessage());
		} finally {
			this.publicacion = new Publicacion();
			this.listarPublicaciones();			
		}

	}
	
	public void listarPublicaciones() {
		try {
			this.publicaciones = this.service.listarPublicacionesPorPublicador(this.us.getPersona());
		} catch (Exception e) {
			//mensajeManager.mostrarMensaje("AVISO", "Error al publicar", "ERROR");
		}
	}

	public List<Publicacion> getPublicaciones() {
		return publicaciones;
	}

	public void setPublicaciones(List<Publicacion> publicaciones) {
		this.publicaciones = publicaciones;
	}

	public Publicacion getPublicacion() {
		return publicacion;
	}

	public void setPublicacion(Publicacion publicacion) {
		this.publicacion = publicacion;
	}
	
	
}
