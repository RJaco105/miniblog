package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

	@Inject
	private IUsuarioService service;
	private String buscarU;
	private List<Usuario> UsLista;

	@PostConstruct
	public void init() {
	}

	public void buscarUsuario() throws Exception {
		try {
			this.UsLista = this.service.buscarUsuario(buscarU);
		} catch (Exception e) {
			throw e;
		}
	}

	public String getBuscarU() {
		return buscarU;
	}

	public void setBuscarU(String buscarU) {
		this.buscarU = buscarU;
	}

	public List<Usuario> getUsLista() {
		return UsLista;
	}

	public void setUsLista(List<Usuario> usLista) {
		UsLista = usLista;
	}

}
